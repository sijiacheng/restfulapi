package cn.sijiacheng;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@MapperScan("cn.sijiacheng.dao")
public class RestApplication {
    public static void main(String[] args) {
        //args为引导参数,可以在此设置启动参数
//        String[] arg1 =new String[1];
//        arg1[0]="--server.port 8085";
        SpringApplication.run(RestApplication.class, args);
        System.out.println("程序在运行");
    }
}
