package cn.sijiacheng.dao;

import cn.sijiacheng.bean.User;
import org.apache.ibatis.annotations.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

/**
 * Title:UserDao
 * Description：用户数据处理接口
 * @author  sijiacheng
 * @date    2022/07/01
 */
@Repository
@Mapper  //表示接口基于此注解实现CRUD
public interface UserDao {

    /**
     * 增加用户
     * @param user
     */
    @Insert("INSERT INTO sys_user(USER_NAME,password,name,age) VALUES (#{USER_NAME},#{password},#{name},#{age})")
    void  addUser(User user);

    /**
     * 用户数据修改
     * @param user
     */
    @Update("UPDATE sys_user SET name=#{name},age=#{age} WHERE id=#{id} AND IS_FROZEN is null")
    void  updateUser(User user);

    /**
     * 用户数据假删除   将删除标记更新为1
     * @param id
     */
    @Delete("UPDATE sys_user SET IS_FROZEN='02'  WHERE id=#{id}")
    void  deleteUser(@Param("id") int  id);


    /**
     * 根据名字查询用户信息
     * @param USER_NAME
     * @return
     */
    @Select("SELECT * FROM sys_user WHERE USER_NAME=#{USER_NAME} AND IS_FROZEN is null")
    User findByName(@Param("USER_NAME") String  USER_NAME);

    /**
     * 根据id查询用户信息
     * @param id
     * @return
     */
    @Select("SELECT * FROM sys_user WHERE id=#{id} AND IS_FROZEN is null")
    User findById(@Param("id") int  id);
    /**
     * 查询所有
     * @return
     */
    @Select("SELECT USER_NAME as login_name  FROM sys_user")
    List<User>  findAll();
}
