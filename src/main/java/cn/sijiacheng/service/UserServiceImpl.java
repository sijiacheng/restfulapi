package cn.sijiacheng.service;

import cn.sijiacheng.bean.User;
import cn.sijiacheng.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Title:UserServiceImpl
 * Description
 * @author  sijiacheng
 * @date  2022/07/01
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Override
    public boolean addUser(User user) {
        boolean flag  =false;
        try {
            userDao.addUser(user);
            flag=true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean updateUser(User user) {
        boolean flag=false;
        try {
            userDao.updateUser(user);
            flag=true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return flag;
    }

    @Override
    public boolean dateleUser(int userId) {
        boolean flag =false;
        try {
            userDao.deleteUser(userId);
            flag=true;
        }catch (Exception e){
            e.printStackTrace();
        }

        return flag;
    }

    @Override
    public User findUserByName(String username) {
        return userDao.findByName(username);
    }

    @Override
    public User findUserById(int id) {
        return userDao.findById(id);
    }

    @Override
    public List<User> findAll() {
        System.out.println("查找");
        return  userDao.findAll();
    }
}
