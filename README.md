# restfulapi

#### 介绍
浏览器访问http://127.0.0.1:8080/restful/findAll(或soapui等工具调用), 
操作数据库CRUD, 并返回json

#### 软件架构说明
1.  springboot(+devtools热部署)
2.  数据库oracle

#### 安装教程

1.  安装maven(建议修改setting.xml),git,idea
2.  复制链接:https://gitee.com/sijiacheng/restfulapi.git 使用IDEA的Git下载
3.  数据库创建表:sys_user,并插入数据

#### 使用说明

1.  启动项目,控制台输出"程序在运行"则启动成功。（启动类RestApplication）
2.  浏览器访问：http://127.0.0.1:8080/restful/findAll，有json字符串,即为成功

#### 参与贡献

1.  sijiacheng(司家成)
2.

#### BUG解决

1.  如果报错:DispatcherServlet wrong
    org.apache.ibatis.binding.BindingException: Invalid bound statement (not found)
    需要检查的步骤：
    1.是否在启动类上扫描了Mapper类 @MapperScan("com.heima.model.mappers")   【注意扫描的包名是否正确】 
    或者 mapper.java文件上使用了注解@Mapper 